package com.fromacion.blockbaste.dto;

import java.util.ArrayList;

public class Tienda {
	
	private String nombre;
	private String direccion;
	
	private ArrayList<Stock> stocks;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	@Override
	public String toString() {
		return "Tienda [nombre=" + nombre + ", direccion=" + direccion + ", stocks=" + stocks + "]";
	}
	public ArrayList<Stock> getStocks() {
		return stocks;
	}
	public void setStocks(ArrayList<Stock> stocks) {
		this.stocks = stocks;
	}
	
	
	

}
