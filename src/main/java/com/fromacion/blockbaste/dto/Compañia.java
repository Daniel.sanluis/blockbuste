package com.fromacion.blockbaste.dto;

public class Compañia {
	
	private String cif;
	private String nombre;
	
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return "Compañia [cif=" + cif + ", nombre=" + nombre + "]";
	}
	
	
	
}
