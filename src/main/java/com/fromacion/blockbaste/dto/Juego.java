package com.fromacion.blockbaste.dto;


import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fromacion.blockbaste.enums.JuegoCategoriaEnum;

public class Juego {
	
	private String titulo;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London")
	private Date fechaLanzamiento;
	private int pegi;
	private JuegoCategoriaEnum categoria;
	
	private ArrayList<Compañia> compañia;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Date getFechaLanzamiento() {
		return fechaLanzamiento;
	}
	public void setFechaLanzamiento(Date fechaLanzamiento) {
		this.fechaLanzamiento = fechaLanzamiento;
	}
	public int getPegi() {
		return pegi;
	}
	public void setPegi(int pegi) {
		this.pegi = pegi;
	}
	public JuegoCategoriaEnum getCategoria() {
		return categoria;
	}
	public void setCategoria(JuegoCategoriaEnum categoria) {
		this.categoria = categoria;
	}
	
	@Override
	public String toString() {
		return "Juego [titulo=" + titulo + ", fechaLanzamiento=" + fechaLanzamiento + ", pegi=" + pegi + ", categoria="
				+ categoria + ", compañia=" + compañia + "]";
	}
	public ArrayList<Compañia> getCompañia() {
		return compañia;
	}
	public void setCompañia(ArrayList<Compañia> compañia) {
		this.compañia = compañia;
	}
	
	
}
