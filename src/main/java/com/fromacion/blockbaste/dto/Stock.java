package com.fromacion.blockbaste.dto;

import javax.validation.constraints.NotEmpty;

import com.fromacion.blockbaste.enums.StockEstadoEnum;

public class Stock {
	@NotEmpty(message="El Estado es obligatorio")
	private StockEstadoEnum estado;
	//@NotEmpty(message="La referencia es obligatora")
	private Integer referencia;
	
	public StockEstadoEnum getEstado() {
		return estado;
	}
	public void setEstado(StockEstadoEnum estado) {
		this.estado = estado;
	}
	public Integer getReferencia() {
		return referencia;
	}
	public void setReferencia(Integer referencia) {
		this.referencia = referencia;
	}
	
	@Override
	public String toString() {
		return "Stock [estado=" + estado + ", referencia=" + referencia + "]";
	}
	

	
	
}
