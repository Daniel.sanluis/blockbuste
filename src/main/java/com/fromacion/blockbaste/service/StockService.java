package com.fromacion.blockbaste.service;

import org.springframework.stereotype.Service;

import com.fromacion.blockbaste.dto.Stock;

@Service
public interface StockService {
	
	public void crearStock(Stock nuevoStock); //añade stock
	
	public Stock buscarStock(Integer referencia ); //muestro stock

	public void borrarStock(Integer referencia); //borrar stock
	
	public void modificarStock(Integer referencia ,Stock stockM); //modificar stock
	

}
