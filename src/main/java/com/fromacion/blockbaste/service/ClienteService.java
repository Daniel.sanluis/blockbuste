package com.fromacion.blockbaste.service;

import org.springframework.stereotype.Service;

import com.fromacion.blockbaste.dto.Cliente;

@Service
public interface ClienteService {
	
	public void crearCliente(Cliente nuevoCliente); //añade cliente
	
	public Cliente buscarCliente(String dni); //muestro cliente	
		
	public void borrarCliente(String dni); //borrar cliente
	
	public void modificarCliente(String dni, Cliente clienteM); //modificar cliente
	
}
