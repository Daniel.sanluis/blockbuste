package com.fromacion.blockbaste.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.fromacion.blockbaste.dto.Compañia;
import com.fromacion.blockbaste.dto.Juego;
import com.fromacion.blockbaste.dto.Tienda;


@Service
public interface TienJueCompService   {
	
	public ArrayList<Tienda> mostrarTienda();

	public ArrayList<Juego> mostrarJuego();
	
	public ArrayList<Compañia> mostrarCompañia();
	

}
