package com.fromacion.blockbaste.service.impl;


import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;


import javax.annotation.PostConstruct;


import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.fromacion.blockbaste.dto.Compañia;
import com.fromacion.blockbaste.dto.Juego;
import com.fromacion.blockbaste.dto.Tienda;
import com.fromacion.blockbaste.service.TienJueCompService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class TienJueCompServiceImp implements TienJueCompService {
	
	public ArrayList<Tienda> listaTienda= new ArrayList<>();
	public ArrayList<Compañia> listaCompañia= new ArrayList<>();
	public ArrayList<Juego> listaJuego= new ArrayList<>();

	public ArrayList<Tienda> mostrarTienda() {		
		return listaTienda;
	}
	
	public ArrayList<Juego> mostrarJuego() {		
		return listaJuego;
	}
	
	public ArrayList<Compañia> mostrarCompañia() {		
		return listaCompañia;
	}
	
	
	
	
	
	@PostConstruct
	public void agregarJsonTienda() {			//Leer una lista desde un json
		JSONParser parser = new JSONParser();
		
		try {
			Object obj = parser.parse(new FileReader("tienda1.json"));
			Gson gson = new Gson(); 
			
			String cadJson = gson.toJson(obj); 
		
			//Tienda tienda = gson.fromJson(cadJson, Tienda.class);  leer un objeto desde un json
			
			//listaTienda.add(tienda);
			

			Type listType = new TypeToken<ArrayList<Tienda>>(){}.getType();
			ArrayList<Tienda> arrayDeJson = gson.fromJson(cadJson, listType);
			listaTienda=arrayDeJson;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
	

	
	@PostConstruct
	public void agregarJsonCompañia() {
		JSONParser parser = new JSONParser();
		
		try {
			Object obj = parser.parse(new FileReader("compañia1.json"));
			Gson gson = new Gson(); 
			
			String cadJson = gson.toJson(obj); 				
			
			//Compañia compañia = gson.fromJson(cadJson, Compañia.class);		
						
			
			//listaCompañia.add(compañia);
			
			Type listType = new TypeToken<ArrayList<Compañia>>(){}.getType();
			ArrayList<Compañia> arrayDeJson = gson.fromJson(cadJson, listType);
			listaCompañia=arrayDeJson;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
	
	@PostConstruct
	public void agregarJsonJuego() {
		JSONParser parser = new JSONParser();
		
		try {
			Object obj = parser.parse(new FileReader("juego1.json"));
			Gson gson = new Gson(); 
			
			String cadJson = gson.toJson(obj); 
		
			//Juego juego = gson.fromJson(cadJson, Juego.class);
			
			//listaJuego.add(juego);
			
			Type listType = new TypeToken<ArrayList<Juego>>(){}.getType();
			ArrayList<Juego> arrayDeJson = gson.fromJson(cadJson, listType);
			listaJuego=arrayDeJson;
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@PostConstruct
	public void juegosComp() {
		
		for(Juego juego: listaJuego) {
			juego.setCompañia(listaCompañia);
		}
		
	}
	
	
	
}
