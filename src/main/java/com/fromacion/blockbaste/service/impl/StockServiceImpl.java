package com.fromacion.blockbaste.service.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fromacion.blockbaste.dto.Cliente;
import com.fromacion.blockbaste.dto.Stock;
import com.fromacion.blockbaste.dto.Tienda;
import com.fromacion.blockbaste.service.StockService;

@Service
public class StockServiceImpl implements StockService{
	
	public ArrayList<Tienda> listaTienda= new ArrayList<>();
	
	public ArrayList<Stock> listaStock= new ArrayList<>();
	
	public void crearStock(Stock nuevoStock) {			//añado un nuevo stock
		listaStock.add(nuevoStock);
		for (Tienda tienda : listaTienda) {
			tienda.setStocks(listaStock);
		}
		
		
	}
	
	/*private void tiendaStock() {
		
		for (Tienda tienda : listaTienda) {
			tienda.setStocks(listaStock);
		}
	}*/
	
	public Stock buscarStock(Integer referencia ) {		//busca por una referencia y muestra el stock
		
		Stock StockNoEncontrado= null;
		
		for(Stock stock : listaStock) {
			if(stock.getReferencia().equals(referencia)) {
				return stock;
			}
		}
		
		return StockNoEncontrado;
	}
	
	
	public void borrarStock(Integer referencia) {		//busca un stock y lo elimina
		
		Stock stock=buscarStock(referencia);
		listaStock.remove(stock);		
	}
	
	
	public void modificarStock(Integer referencia ,Stock stockM) {		//modifica un Stock
		
		Stock stock = buscarStock(referencia);
		
		if(stock.getEstado() != stockM.getEstado()) {
			stock.setEstado(stockM.getEstado());			
		}
		
		if(stock.getReferencia() != stockM.getReferencia()) {
			stock.setReferencia(stockM.getReferencia());
		}		
	}

	
	 // calcularEdad por la fecha de nacimiento
	 private void calcularEdad(Cliente cliente) {
		 int edadCliente = Period.between(cliente.getFechaNacimiento(), LocalDate.now()).getYears();
		
	 }
}
