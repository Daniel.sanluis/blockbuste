package com.fromacion.blockbaste.service.impl;


import java.util.ArrayList;
import org.springframework.stereotype.Service;

import com.fromacion.blockbaste.dto.Cliente;
import com.fromacion.blockbaste.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {
	
	public ArrayList<Cliente> listaCliente= new ArrayList<>();
	
	public void crearCliente(Cliente nuevoCliente) {		//crea un cliente	
		listaCliente.add(nuevoCliente);
	}

	
	public Cliente buscarCliente(String dni) {			//busca un cliente y lo muestra
		
		Cliente clienteNoEncontrado= null;
		
		for(Cliente cliente : listaCliente) {
			if(cliente.getDni().equalsIgnoreCase(dni)) {
				return cliente;
			}
		}
		
		return clienteNoEncontrado;
	}	
	

	public void borrarCliente(String dni) {		//borra un cliente
		
		Cliente cliente= buscarCliente(dni);
		listaCliente.remove(cliente);		
	}
	
	
	public void modificarCliente(String dni,Cliente clienteM) {		//modifica a un cliente
		
		Cliente cliente= buscarCliente(dni);
		
		if(cliente.getNombre() != clienteM.getNombre()) {
			cliente.setNombre(clienteM.getNombre());
		}
		
		if(cliente.getCorreo() != clienteM.getCorreo()) {
			cliente.setCorreo(clienteM.getCorreo());
		}
		
		if(cliente.getFechaNacimiento() != clienteM.getFechaNacimiento()) {
			cliente.setFechaNacimiento(clienteM.getFechaNacimiento());
		}
		
		if(cliente.getDni() != clienteM.getDni()) {
			cliente.setDni(clienteM.getDni());
		}
	}
	
	
	
	
	
}
