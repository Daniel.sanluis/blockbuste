package com.fromacion.blockbaste.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fromacion.blockbaste.dto.Stock;
import com.fromacion.blockbaste.service.StockService;

@RestController
public class StockController {
	
	@Autowired
	private StockService stockService;
	
	@PostMapping("/crearStock")
	public void crearStock(@RequestBody Stock nuevoStock) {
		stockService.crearStock(nuevoStock);		
	}
	
	@GetMapping("/buscarStock")
	public ResponseEntity<Stock> buscarStock(@RequestParam Integer referencia){
		return new ResponseEntity<Stock>(stockService.buscarStock(referencia), HttpStatus.OK);	
	}
	
	@PostMapping("/borrarStock")
	public void borrarStock(@RequestParam Integer referencia) {
		stockService.borrarStock(referencia);;	
	}

	@PostMapping("/modificarStock")
	public void modificarStock(@RequestParam Integer referencia ,@RequestBody @Valid Stock stockM) {
		stockService.modificarStock(referencia, stockM);
	}
	
}
