package com.fromacion.blockbaste.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fromacion.blockbaste.dto.Cliente;
import com.fromacion.blockbaste.service.ClienteService;

@RestController
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@PostMapping("/crearCliente")
	public void crearCliente(@RequestBody @Valid Cliente nuevoCliente) {
		clienteService.crearCliente(nuevoCliente);		
	}
	
	@GetMapping("/buscarCliente")
	public ResponseEntity<Cliente> buscarCliente(@RequestParam String dni){
		return new ResponseEntity<Cliente>(clienteService.buscarCliente(dni), HttpStatus.OK);	
	}
	
	@PostMapping("/borrarCliente")
	public void borrarCliente(@RequestParam String dni) {
		clienteService.borrarCliente(dni);
	}

	@PostMapping("/modificarCliente")
	public void modificarCliente(@RequestParam String dni,@RequestBody @Valid Cliente clienteM) {
		clienteService.modificarCliente(dni, clienteM);
	}
}
