package com.fromacion.blockbaste.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

import com.fromacion.blockbaste.dto.Compañia;
import com.fromacion.blockbaste.dto.Juego;

import com.fromacion.blockbaste.dto.Tienda;
import com.fromacion.blockbaste.service.TienJueCompService;

@RestController
public class TienJueCompController {
	
	@Autowired
	private TienJueCompService tienJueCompService ;
		
	
	@GetMapping("/mostrarTienda")
	public ResponseEntity<ArrayList<Tienda>> mostrarTienda(){
		return new ResponseEntity<ArrayList<Tienda>>(tienJueCompService.mostrarTienda(), HttpStatus.OK);	
	}
	
	@GetMapping("/mostrarJuego")
	public ResponseEntity<ArrayList<Juego>> mostrarJuego(){
		return new ResponseEntity<ArrayList<Juego>>(tienJueCompService.mostrarJuego(), HttpStatus.OK);	
	}
	
	@GetMapping("/mostrarCompania")
	public ResponseEntity<ArrayList<Compañia>> mostrarCompañia(){
		return new ResponseEntity<ArrayList<Compañia>>(tienJueCompService.mostrarCompañia(), HttpStatus.OK);	
	}
}
