package com.fromacion.blockbaste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlockbasteApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlockbasteApplication.class, args);
	}

}
